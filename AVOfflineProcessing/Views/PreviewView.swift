//
//  PreviewView.swift
//  AVOfflineProcessing
//
//  Created by Maxim Pavlov on 06.08.2018.
//  Copyright © 2018 maximpavlov. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PreviewView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.clipsToBounds = true
        self.previewLayer.cornerRadius = 6.0
        self.previewLayer.videoGravity = .resizeAspectFill
    }
    
    var previewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
    
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
}
