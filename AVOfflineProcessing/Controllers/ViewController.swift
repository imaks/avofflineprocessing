//
//  ViewController.swift
//  AVOfflineProcessing
//
//  Created by Maxim Pavlov on 06.08.2018.
//  Copyright © 2018 maximpavlov. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    var mediaCapturer: MediaCapturer!
    let mediaProcessor = MediaProcessor()
    
    @IBOutlet var previewView: PreviewView!
    @IBOutlet var mediaPlayerView: PlayerView!
    @IBOutlet var recordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mediaCapturer = MediaCapturer.init(delegate: self)
        mediaCapturer.startCameraCapture(to: previewView.previewLayer) { error in
            if error != nil {
                self.recordButton.isEnabled = false
                self.recordButton.setTitle("App needs camera permission", for: .normal)
            }
        }
    }
    
    override var shouldAutorotate: Bool {
        return !mediaCapturer.isRecording
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
    
    @IBAction func startButtonPressed(sender: UIButton) {
        var title: String?
        if mediaCapturer.isRecording {
            title = "Start recording"
            mediaCapturer.stopRecording()
        } else {
            title = "Stop recording"
            mediaCapturer.startRecording(to: URLForMovie("CameraRecording"))
        }
        sender.setTitle(title, for: .normal)
    }
}

extension ViewController: MediaCapturerDelegate {
    func mediaCapturer(_ sender: MediaCapturer, didFinishRecordingTo movieURL: URL) {
        mediaPlayerView.isHidden = false
        previewView.isHidden = true
        recordButton.isHidden = true
        
        mediaProcessor.process(movieAt: movieURL, to: URLForMovie("ProcessedCameraRecording"), outputLayer: mediaPlayerView.playerLayer) { error in
            if let error = error {
                print("\(error)")
            }
            
            self.mediaPlayerView.isHidden = true
            self.previewView.isHidden = false
            self.recordButton.isHidden = false
        }
    }
    
    func mediaCapturer(_ sender: MediaCapturer, wasInterruptedWith reason: AVCaptureSession.InterruptionReason) {
        
    }
    
    func mediaCapturerInterruptionEnded(_ sender: MediaCapturer) {
        
    }
    
    func mediaCapturer(_ sender: MediaCapturer, didReceive runtimeError: AVError) {
        
    }
}

private extension ViewController {
    func URLForMovie(_ movieName: String) -> URL {
        let moviePath = (NSTemporaryDirectory() as NSString).appendingPathComponent((movieName as NSString).appendingPathExtension("mov")!)
        return URL(fileURLWithPath: moviePath)
    }
}
