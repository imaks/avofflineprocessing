//
//  MediaCapturer.swift
//  AVOfflineProcessing
//
//  Created by Maxim Pavlov on 06.08.2018.
//  Copyright © 2018 maximpavlov. All rights reserved.
//

import AVFoundation
import UIKit

enum MediaCapturerError: Error {
    case inputUnavailable
    case outputUnavailable
    case notAuthorized
    case systemFailed(underlyingError: Error)
}

protocol MediaCapturerDelegate: AnyObject {
    func mediaCapturer(_ sender: MediaCapturer, didFinishRecordingTo movieURL: URL)
    func mediaCapturer(_ sender: MediaCapturer, wasInterruptedWith reason: AVCaptureSession.InterruptionReason)
    func mediaCapturerInterruptionEnded(_ sender: MediaCapturer)
    func mediaCapturer(_ sender: MediaCapturer, didReceive runtimeError: AVError)
}

/**
 `MediaCapturer` is responsible for capturing camera video with audio, displaying it into passed AVCaptureVideoPreviewLayer and recording to a file.
 Its goal is to provide simple, clean interface for working with these tasks, not dealing with AVFoundation internals.
 Maybe it's too much for one class but splitting it into smaller classes won't help much for now.
 Orientation changes are handled internally, so class clients don't need to worry about it.
 System interruption notifications and runtime errors are observed and passed to `MediaCapturerDelegate`.
 */
class MediaCapturer: NSObject {
    private let session = AVCaptureSession()
    private let movieFileOutput = AVCaptureMovieFileOutput()
    private let sessionQueue = DispatchQueue(label: "com.maximpavlov.mediaCaptureSessionQueue");
    private var videoPreviewConnection: AVCaptureConnection?
    private weak var delegate: MediaCapturerDelegate?
    
    convenience init(delegate: MediaCapturerDelegate?) {
        self.init()
        
        self.delegate = delegate
    }
    
    override init() {
        super.init()
        
        addObservers()
    }
    
    func startCameraCapture(to layer:AVCaptureVideoPreviewLayer, completion: ((_ error: MediaCapturerError?) -> Void)? = nil) {
        guard !session.isRunning else {
            completion?(nil)
            return;
        }
        
        var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
        if let videoOrientation = AVCaptureVideoOrientation(interfaceOrientation: UIApplication.shared.statusBarOrientation) {
            initialVideoOrientation = videoOrientation
        }
        
        func startSession() {
            sessionQueue.async {
                self.session.startRunning()
                
                DispatchQueue.main.async {
                    if let videoPreviewConnection = self.preparePreviewCaptureConnection(with: layer) {
                        videoPreviewConnection.videoOrientation = initialVideoOrientation
                    }
                    completion?(nil)
                }
            }
        }
        
        requestAuthorization { error in
            if let error = error {
                DispatchQueue.main.async {
                    completion?(error)
                }
                return
            }
            
            let isSessionConfigured = self.session.inputs.count > 0 && self.session.outputs.count > 0
            if isSessionConfigured {
                startSession()
            } else {
                self.setupCaptureSession(completion: { error in
                    if let error = error {
                        DispatchQueue.main.async {
                            completion?(error)
                        }
                        return
                    }
                    
                    startSession()
                })
            }
        }
    }
    
    var isRecording: Bool {
        return movieFileOutput.isRecording
    }
    
    func startRecording(to outputMovieURL:URL) {
        guard !isRecording,
            let movieOutputConnection = self.movieFileOutput.connection(with: .video),
            let videoPreviewConnection = videoPreviewConnection else {
                return
        }
        
        self.cleanUp(at: outputMovieURL)
        movieOutputConnection.videoOrientation = videoPreviewConnection.videoOrientation
        self.movieFileOutput.startRecording(to: outputMovieURL, recordingDelegate: self)
    }
    
    func stopRecording() {
        self.movieFileOutput.stopRecording()
    }
}

extension MediaCapturer: AVCaptureFileOutputRecordingDelegate {
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        var success = true
        
        if error != nil {
            success = (((error! as NSError).userInfo[AVErrorRecordingSuccessfullyFinishedKey] as AnyObject).boolValue)!
        }
        
        if success {
            DispatchQueue.main.async {
                self.delegate?.mediaCapturer(self, didFinishRecordingTo: outputFileURL)
            }
        } else {
            cleanUp(at: outputFileURL)
        }
    }
}

private extension MediaCapturer {
    
    // MARK: AVFoundation setup

    func requestAuthorization(completion: @escaping (_ error: MediaCapturerError?) -> Void) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            completion(nil)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                completion(granted ? nil : MediaCapturerError.notAuthorized)
            })
        case .denied, .restricted:
            completion(MediaCapturerError.notAuthorized)
        }
    }
    
    func setupCaptureSession(completion: @escaping (_ error: MediaCapturerError?) -> Void) {
        sessionQueue.async {
            do {
                self.session.beginConfiguration()
                try self.addVideoInput()
                try self.addAudioInput()
                try self.addMovieOutput()
                self.session.commitConfiguration()
                completion(nil)
            } catch let error as MediaCapturerError {
                completion(error)
            } catch {
                completion(MediaCapturerError.systemFailed(underlyingError: error))
            }
        }
    }
    
    func addVideoInput() throws {
        if let videoCaptureDevice = availableVideoCaptureDevice() {
            try addInput(captureDevice: videoCaptureDevice)
        } else {
            throw MediaCapturerError.inputUnavailable
        }
    }
    
    func addAudioInput() throws {
        if let audioCaptureDevice = availableAudioCaptureDevice() {
            try addInput(captureDevice: audioCaptureDevice)
        } else {
            throw MediaCapturerError.inputUnavailable
        }
    }
    
    // Tries to choose front camera first, if possible
    func availableVideoCaptureDevice() -> AVCaptureDevice? {
        if let frontCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) {
            return frontCameraDevice
        } else if let backCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) {
            return backCameraDevice
        }
        return nil
    }
    
    func availableAudioCaptureDevice() -> AVCaptureDevice? {
        return AVCaptureDevice.default(for: .audio)
    }
    
    func addInput(captureDevice: AVCaptureDevice) throws {
        let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
        if session.canAddInput(deviceInput) {
            session.addInput(deviceInput)
        } else {
            throw MediaCapturerError.inputUnavailable
        }
    }
    
    func addMovieOutput() throws {
        if session.canAddOutput(movieFileOutput) {
            session.addOutput(movieFileOutput)
            if session.canSetSessionPreset(.high) {
                session.sessionPreset = .high
            } else {
                throw MediaCapturerError.outputUnavailable
            }
            if let connection = movieFileOutput.connection(with: .video) {
                if connection.isVideoStabilizationSupported {
                    connection.preferredVideoStabilizationMode = .auto
                }
            }
        } else {
            throw MediaCapturerError.outputUnavailable
        }
    }
    
    // Creating AVCaptureConnection here manually to manage all rotations internally
    // AVCaptureVideoPreviewLayer creates connections itself by default but this way rotation handling code will be spread among classes.
    func preparePreviewCaptureConnection(with layer: AVCaptureVideoPreviewLayer) -> AVCaptureConnection? {
        if let videoPreviewConnection = videoPreviewConnection {
            return videoPreviewConnection
        }
        
        layer.setSessionWithNoConnection(session)
        
        guard let input = session.inputs.filter({ input in
            let deviceInput = input as! AVCaptureDeviceInput
            return deviceInput.device.hasMediaType(.video)
        }).first else {
            return nil
        }
        
        guard let port = input.ports.filter({
            $0.mediaType == .video
        }).first else {
            return nil
        }
        
        let connection = AVCaptureConnection.init(inputPort: port, videoPreviewLayer: layer)
        guard session.canAdd(connection) else {
            return nil
        }
        
        session.add(connection)
        videoPreviewConnection = connection
        return videoPreviewConnection
    }
    
    func cleanUp(at URL: URL) {
        let path = URL.path
        if FileManager.default.fileExists(atPath: path){
            do {
                try FileManager.default.removeItem(atPath: path)
            } catch {
                print("Could not remove file at url: \(URL)")
            }
        }
    }
    
    // MARK: Notifications
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(sessionRuntimeError), name: .AVCaptureSessionRuntimeError, object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionWasInterrupted), name: .AVCaptureSessionWasInterrupted, object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionInterruptionEnded), name: .AVCaptureSessionInterruptionEnded, object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(orientationChanged), name: .UIDeviceOrientationDidChange, object: UIDevice.current)
    }
    
    @objc
    func sessionRuntimeError(notification: NSNotification) {
        guard let error = notification.userInfo?[AVCaptureSessionErrorKey] as? AVError else { return }
        DispatchQueue.main.async {
            self.delegate?.mediaCapturer(self, didReceive: error)
        }
    }
    
    @objc
    func sessionWasInterrupted(notification: NSNotification) {
        if let userInfoValue = notification.userInfo?[AVCaptureSessionInterruptionReasonKey] as AnyObject?,
            let reasonIntegerValue = userInfoValue.integerValue,
            let reason = AVCaptureSession.InterruptionReason(rawValue: reasonIntegerValue) {
            DispatchQueue.main.async {
                self.delegate?.mediaCapturer(self, wasInterruptedWith: reason)
            }
        }
    }
    
    @objc
    func sessionInterruptionEnded(notification: NSNotification) {
        DispatchQueue.main.async {
            self.delegate?.mediaCapturerInterruptionEnded(self)
        }
    }
    
    @objc func orientationChanged(notification: NSNotification) {
        guard !isRecording else {
            return
        }
        
        if let videoPreviewConnection = videoPreviewConnection {
            let deviceOrientation = UIDevice.current.orientation
            guard let newVideoOrientation = AVCaptureVideoOrientation(deviceOrientation: deviceOrientation), deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
                return
            }
            
            DispatchQueue.main.async {
                videoPreviewConnection.videoOrientation = newVideoOrientation
            }
        }
    }
}

private extension AVCaptureVideoOrientation {
    init?(deviceOrientation: UIDeviceOrientation) {
        switch deviceOrientation {
        case .portrait: self = .portrait
        case .portraitUpsideDown: self = .portraitUpsideDown
        case .landscapeLeft: self = .landscapeRight
        case .landscapeRight: self = .landscapeLeft
        default: return nil
        }
    }
    
    init?(interfaceOrientation: UIInterfaceOrientation) {
        switch interfaceOrientation {
        case .portrait: self = .portrait
        case .portraitUpsideDown: self = .portraitUpsideDown
        case .landscapeLeft: self = .landscapeLeft
        case .landscapeRight: self = .landscapeRight
        default: return nil
        }
    }
}
