//
//  PlayerView.swift
//  AVOfflineProcessing
//
//  Created by Maxim Pavlov on 06.08.2018.
//  Copyright © 2018 maximpavlov. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PlayerView: UIView {
    
    var playerLayer: AVSampleBufferDisplayLayer {
        return layer as! AVSampleBufferDisplayLayer
    }
    
    override class var layerClass: AnyClass {
        return AVSampleBufferDisplayLayer.self
    }
}
