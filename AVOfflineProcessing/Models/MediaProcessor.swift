//
//  MediaViewer.swift
//  AVOfflineProcessing
//
//  Created by Maxim Pavlov on 06.08.2018.
//  Copyright © 2018 maximpavlov. All rights reserved.
//

import AVFoundation
import UIKit
import Photos

enum MediaProcessorError: Error {
    case assetReaderInitializationFailed
    case assetWriterInitializationFailed
    case imageCreationFailed
    case systemFailed(underlyingError: Error?)
}

/**
 `MediaProcessor` is responsible for editing provided video, displaying it inside `AVSampleBufferDisplayLayer` and saving edited version into a photo library.
  We are using `AVSampleBufferDisplayLayer` because it is tailored for that exact purpose, `UIImageView` won't fit here.
  Editing is handled with filters applied via `AVMutableVideoComposition`, this way we get maximum performance.
 */
class MediaProcessor {
    private var assetReader: AVAssetReader?
    private var assetWriter: AVAssetWriter?
    
    private let workerQueue = DispatchQueue(label: "com.maximpavlov.workerQueue");
    private let audioWriterQueue = DispatchQueue(label: "com.maximpavlov.audioWriterQueue");
    private let videoWriterQueue = DispatchQueue(label: "com.maximpavlov.videoWriterQueue");
    
    func process(movieAt URL: URL, to outputMovieURL: URL, outputLayer: AVSampleBufferDisplayLayer, completion: @escaping (_ error: MediaProcessorError?) -> Void) {
        outputLayer.controlTimebase = makeLayerControlTimebase()
        outputLayer.flushAndRemoveImage()
        
        let asset = AVURLAsset(url: URL, options: nil)
        let tracksKey = "tracks"
        asset.loadValuesAsynchronously(forKeys: [tracksKey]) {
            self.workerQueue.async {
                do {
                    var trackLoadingError: NSError?
                    guard asset.statusOfValue(forKey: tracksKey, error: &trackLoadingError) == .loaded else {
                        completion(MediaProcessorError.systemFailed(underlyingError: trackLoadingError))
                        return
                    }
                    
                    try self.process(preparedAsset: asset, to: outputMovieURL, outputLayer: outputLayer, completion: completion)
                } catch let error as MediaProcessorError {
                    completion(error)
                } catch {
                    completion(MediaProcessorError.systemFailed(underlyingError: error))
                }
            }
        }
    }
}

private extension MediaProcessor {
    func process(preparedAsset: AVURLAsset, to outputMovieURL: URL, outputLayer: AVSampleBufferDisplayLayer, completion: @escaping (_ error: MediaProcessorError?) -> Void) throws {
        let (readerLayerOutput, readerVideoOutput, readerAudioOutput) = try self.setupAssetReader(with: preparedAsset)
        
        guard let assetReader = self.assetReader, assetReader.startReading() else {
            throw MediaProcessorError.systemFailed(underlyingError: self.assetReader?.error)
        }
        
        self.cleanUp(at: outputMovieURL)
        // Specifying video size to match video composition render size as otherwise final video may have wrong dimensions
        let videoSize = readerVideoOutput.videoComposition!.renderSize
        let (writerVideoInput, writerAudioInput) = try self.setupAssetWriter(with: outputMovieURL, videoSize: videoSize)
        
        guard let assetWriter = self.assetWriter, assetWriter.startWriting() else {
            throw MediaProcessorError.systemFailed(underlyingError: self.assetWriter?.error)
        }
        assetWriter.startSession(atSourceTime: kCMTimeZero)
        
        let processingUnits: [(AVAssetReaderOutput, AVAssetWriterInput, DispatchQueue)] = [(readerVideoOutput, writerVideoInput, self.videoWriterQueue),
                                                                                           (readerAudioOutput, writerAudioInput, self.audioWriterQueue)]
        let dispatchGroup = DispatchGroup.init()
        for (readerOutput, writerInput, queue) in processingUnits {
            dispatchGroup.enter()
            writerInput.requestMediaDataWhenReady(on: queue, using: {
                while writerInput.isReadyForMoreMediaData {
                    guard let sampleBuffer = readerOutput.copyNextSampleBuffer(), writerInput.append(sampleBuffer) else {
                        writerInput.markAsFinished()
                        dispatchGroup.leave()
                        break
                    }
                }
            })
        }
        
        dispatchGroup.enter()
        outputLayer.requestMediaDataWhenReady(on: DispatchQueue.main, using: {
            while outputLayer.isReadyForMoreMediaData {
                if let sampleBuffer = readerLayerOutput.copyNextSampleBuffer() {
                    outputLayer.enqueue(sampleBuffer)
                } else {
                    // Seems like there is no way to know the exact moment when AVSampleBufferDisplayLayer finishes rendering, its `status` parameter never updates to .unknown from .rendering.
                    // This way we'll just report completion a bit after buffering finishes
                    outputLayer.stopRequestingMediaData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                        dispatchGroup.leave()
                    })
                    break
                }
            }
        })
        
        dispatchGroup.notify(queue: self.workerQueue, execute: {
            assetWriter.finishWriting {
                self.saveMovieToPhotoLibrary(movieURL: outputMovieURL)
                
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        })
    }
    
    func saveMovieToPhotoLibrary(movieURL: URL) {
        PHPhotoLibrary.requestAuthorization { status in
            if status == .authorized {
                PHPhotoLibrary.shared().performChanges({
                    let options = PHAssetResourceCreationOptions()
                    options.shouldMoveFile = true
                    let creationRequest = PHAssetCreationRequest.forAsset()
                    creationRequest.addResource(with: .video, fileURL: movieURL, options: options)
                })
            }
        }
    }
}

private extension MediaProcessor {
    func setupAssetReader(with asset:AVURLAsset) throws -> (AVAssetReaderVideoCompositionOutput, AVAssetReaderVideoCompositionOutput, AVAssetReaderTrackOutput){
        
        func makeVideoCompositionOutput() -> AVAssetReaderVideoCompositionOutput {
            let output = AVAssetReaderVideoCompositionOutput(videoTracks: asset.tracks(withMediaType: .video), videoSettings: nil)
            output.alwaysCopiesSampleData = false
            output.videoComposition = makeVideoComposition(with: asset)
            return output
        }
        
        let assetReader = try AVAssetReader(asset: asset)
        let readerLayerOutput = makeVideoCompositionOutput()
        let readerVideoOutput = makeVideoCompositionOutput()
        let readerAudioOutput = AVAssetReaderTrackOutput(track: asset.tracks(withMediaType: .audio).first!, outputSettings: nil)
        readerAudioOutput.alwaysCopiesSampleData = false
        
        for readerOutput in [readerLayerOutput, readerVideoOutput, readerAudioOutput] {
            if assetReader.canAdd(readerOutput) {
                assetReader.add(readerOutput)
            } else {
                throw MediaProcessorError.assetReaderInitializationFailed
            }
        }
        
        self.assetReader = assetReader
        return (readerLayerOutput, readerVideoOutput, readerAudioOutput)
    }
    
    func setupAssetWriter(with outputMovieURL: URL, videoSize: CGSize) throws -> (AVAssetWriterInput, AVAssetWriterInput) {
        let assetWriter = try AVAssetWriter(outputURL: outputMovieURL, fileType: .mov)
        let audioInput = AVAssetWriterInput(mediaType: .audio, outputSettings: nil)
        let videoInput = AVAssetWriterInput(mediaType: .video, outputSettings: [AVVideoCodecKey: AVVideoCodecH264,
                                                                                AVVideoWidthKey: videoSize.width,
                                                                                AVVideoHeightKey: videoSize.height])
        for writerInput in [videoInput, audioInput] {
            if assetWriter.canAdd(writerInput) {
                assetWriter.add(writerInput)
            } else {
                throw MediaProcessorError.assetWriterInitializationFailed
            }
        }
        
        self.assetWriter = assetWriter
        return (videoInput, audioInput)
    }
    
    func makeVideoComposition(with asset:AVAsset) -> AVVideoComposition {
        let track = asset.tracks(withMediaType: .video).first!
        
        let minFrameDuration = track.minFrameDuration
        let videoComposition = AVMutableVideoComposition(asset: asset, applyingCIFiltersWithHandler: { request in
            let source = request.sourceImage.clampedToExtent()
            let frameNumber = Int(CMTimeGetSeconds(CMTimeMultiplyByRatio(request.compositionTime, minFrameDuration.timescale, Int32(minFrameDuration.value))))
            
            guard let textImage = self.makeTextImage(frameNumber: frameNumber + 1, renderSize: request.renderSize) else {
                request.finish(with: MediaProcessorError.imageCreationFailed)
                return
            }
            
            request.finish(with: textImage.composited(over: source), context: nil)
        })
        videoComposition.frameDuration = minFrameDuration
        return videoComposition.copy() as! AVVideoComposition
    }
    
    func makeTextImage(frameNumber: Int, renderSize: CGSize) -> CIImage? {
        // Setting static context size for simplicity, but ideally we can calculate text size upfront and assign it to context size
        let size = CGSize(width: 50, height: 50)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let frameNumberString = NSString(string: String(frameNumber))
        frameNumberString.draw(at: CGPoint.zero, withAttributes: [NSAttributedStringKey.foregroundColor: UIColor.red,
                                                                  NSAttributedStringKey.font: UIFont.systemFont(ofSize: 32)])
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        return CIImage(image: image)?.transformed(by: CGAffineTransform(translationX: (renderSize.width - size.width * UIScreen.main.scale), y: 0.0))
    }
    
    func makeLayerControlTimebase() -> CMTimebase? {
        var controlTimebase: CMTimebase?
        CMTimebaseCreateWithMasterClock(kCFAllocatorDefault, CMClockGetHostTimeClock(),  &controlTimebase)
        CMTimebaseSetTime(controlTimebase!, CMTimeMake(0, 600))
        CMTimebaseSetRate(controlTimebase!, 1.0)
        return controlTimebase
    }
    
    func cleanUp(at URL: URL) {
        let path = URL.path
        if FileManager.default.fileExists(atPath: path){
            do {
                try FileManager.default.removeItem(atPath: path)
            } catch {
                print("Could not remove file at url: \(URL)")
            }
        }
    }
}
